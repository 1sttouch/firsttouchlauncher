//
//  main.m
//  firsturl
//
//  Created by Mike Zriel on 26/04/2016.
//  Copyright © 2016 1st Touch Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

//
//  ViewController.h
//  firsturl
//
//  Created by Mike Zriel on 26/04/2016.
//  Copyright © 2016 1st Touch Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *output;

@end


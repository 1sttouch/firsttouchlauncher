//
//  ViewController.m
//  firsturl
//
//  Created by Mike Zriel on 26/04/2016.
//  Copyright © 2016 1st Touch Ltd. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLabel:) name:@"handleOpenURL" object:nil];
}


-(void) updateLabel: (NSNotification *) notification {
    
    if (notification.object) {
        self.output.text = notification.object;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)buttonTestPressed:(id)sender {
    
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"firsttouch://OpenTask?id=1"]];
}
- (IBAction)createTaskButtonPressed:(id)sender {
    
    NSString *string = @"firsttouch://CreateTask?formType=Gas&data=%5BName%3A%22Customer+Name%22%2C+Value%3A%22Mrs+Miggins%22%7D%2C%0D%0A%7BName%3A%22Address+Line+1%22%2CValue%3A%221+Somestreet%22%7D%5D";
    
   [[UIApplication sharedApplication] openURL:[NSURL URLWithString:string]];

    
}
- (IBAction)clearButtonPressed:(id)sender {
    self.output.text = @"";
}

@end
